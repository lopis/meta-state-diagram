package model;
//
// THIS FILE IS AUTOMATICALLY GENERATED!!
//
// Generated at 2012-12-06 by the VDM++ to JAVA Code Generator
// (v8.1.1b - Thu 30-Oct-2008 14:59:38)
//
// Supported compilers: jdk 1.4/1.5/1.6
//

// ***** VDMTOOLS START Name=HeaderComment KEEP=NO
// ***** VDMTOOLS END Name=HeaderComment

// ***** VDMTOOLS START Name=package KEEP=NO
// ***** VDMTOOLS END Name=package

// ***** VDMTOOLS START Name=imports KEEP=NO

import jp.co.csk.vdm.toolbox.VDM.*;
import java.util.*;
import jp.co.csk.vdm.toolbox.VDM.jdk.*;
// ***** VDMTOOLS END Name=imports



public class Choice implements EvaluatePP {

// ***** VDMTOOLS START Name=vdmComp KEEP=NO
  static UTIL.VDMCompare vdmComp = new UTIL.VDMCompare();
// ***** VDMTOOLS END Name=vdmComp

// ***** VDMTOOLS START Name=ifFalse KEEP=NO
  private volatile Transition ifFalse = null;
// ***** VDMTOOLS END Name=ifFalse

// ***** VDMTOOLS START Name=ifTrue KEEP=NO
  private volatile Transition ifTrue = null;
// ***** VDMTOOLS END Name=ifTrue

// ***** VDMTOOLS START Name=condition KEEP=NO
  private volatile String condition = null;
// ***** VDMTOOLS END Name=condition

// ***** VDMTOOLS START Name=sentinel KEEP=NO
  volatile Sentinel sentinel;
// ***** VDMTOOLS END Name=sentinel


// ***** VDMTOOLS START Name=ChoiceSentinel KEEP=NO
  class ChoiceSentinel extends Sentinel {

    public final int nr_functions = 0;


    public ChoiceSentinel () throws CGException {}


    public ChoiceSentinel (EvaluatePP instance) throws CGException {
      init(nr_functions, instance);
    }

  }
// ***** VDMTOOLS END Name=ChoiceSentinel
;

// ***** VDMTOOLS START Name=evaluatePP#1|int KEEP=NO
  public Boolean evaluatePP (int fnr) throws CGException {
    return new Boolean(true);
  }
// ***** VDMTOOLS END Name=evaluatePP#1|int


// ***** VDMTOOLS START Name=setSentinel KEEP=NO
  public void setSentinel () {
    try {
      sentinel = new ChoiceSentinel(this);
    }
    catch (CGException e) {
      System.out.println(e.getMessage());
    }
  }
// ***** VDMTOOLS END Name=setSentinel


// ***** VDMTOOLS START Name=vdm_init_Choice KEEP=NO
  private void vdm_init_Choice () throws CGException {
    try {
      setSentinel();
    }
    catch (Exception e){

      e.printStackTrace(System.out);
      System.out.println(e.getMessage());
    }
  }
// ***** VDMTOOLS END Name=vdm_init_Choice


// ***** VDMTOOLS START Name=Choice KEEP=NO
  public Choice () throws CGException {
    vdm_init_Choice();
  }
// ***** VDMTOOLS END Name=Choice

}
;
