package model;
//
// THIS FILE IS AUTOMATICALLY GENERATED!!
//
// Generated at 2012-12-06 by the VDM++ to JAVA Code Generator
// (v8.1.1b - Thu 30-Oct-2008 14:59:38)
//
// Supported compilers: jdk 1.4/1.5/1.6
//

// ***** VDMTOOLS START Name=HeaderComment KEEP=NO
// ***** VDMTOOLS END Name=HeaderComment

// ***** VDMTOOLS START Name=package KEEP=NO
// ***** VDMTOOLS END Name=package

// ***** VDMTOOLS START Name=imports KEEP=NO

import jp.co.csk.vdm.toolbox.VDM.*;
import java.util.*;
import jp.co.csk.vdm.toolbox.VDM.jdk.*;
// ***** VDMTOOLS END Name=imports



public class Terminate extends State {

// ***** VDMTOOLS START Name=vdmComp KEEP=NO
  static UTIL.VDMCompare vdmComp = new UTIL.VDMCompare();
// ***** VDMTOOLS END Name=vdmComp


// ***** VDMTOOLS START Name=TerminateSentinel KEEP=NO
  class TerminateSentinel extends StateSentinel {

    public final int nr_functions = 5;


    public TerminateSentinel () throws CGException {}


    public TerminateSentinel (EvaluatePP instance) throws CGException {
      init(nr_functions, instance);
    }

  }
// ***** VDMTOOLS END Name=TerminateSentinel
;

// ***** VDMTOOLS START Name=setSentinel KEEP=NO
  public void setSentinel () {
    try {
      sentinel = new TerminateSentinel(this);
    }
    catch (CGException e) {
      System.out.println(e.getMessage());
    }
  }
// ***** VDMTOOLS END Name=setSentinel


// ***** VDMTOOLS START Name=vdm_init_Terminate KEEP=NO
  private void vdm_init_Terminate () throws CGException {}
// ***** VDMTOOLS END Name=vdm_init_Terminate


// ***** VDMTOOLS START Name=Terminate KEEP=NO
  public Terminate () throws CGException {
    vdm_init_Terminate();
  }
// ***** VDMTOOLS END Name=Terminate

}
;
