package view;

public class Node{

	StateNode sn;
	
	public Node(StateNode s) {
		sn = s;
	}
	
	@Override
	public String toString(){
		return sn.toString();
	}
}
