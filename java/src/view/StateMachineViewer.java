/* ==========================================
 * JGraphT : a free Java graph-theory library
 * ==========================================
 *
 * Project Info:  http://jgrapht.sourceforge.net/
 * Project Creator:  Barak Naveh (http://sourceforge.net/users/barak_naveh)
 *
 * (C) Copyright 2003-2008, by Barak Naveh and Contributors.
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
/* ----------------------
 * JGraphAdapterDemo.java
 * ----------------------
 * (C) Copyright 2003-2008, by Barak Naveh and Contributors.
 *
 * Original Author:  Barak Naveh
 * Contributor(s):   -
 *
 * $Id: JGraphAdapterDemo.java 725 2010-11-26 01:24:28Z perfecthash $
 *
 * Changes
 * -------
 * 03-Aug-2003 : Initial revision (BN);
 * 07-Nov-2003 : Adaptation to JGraph 3.0 (BN);
 *
 */
package view;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.*;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.*;

import jp.co.csk.vdm.toolbox.VDM.CGException;


import model.Final;
import model.Initial;
import model.Machine;
import model.State;

import org.jgraph.*;
import org.jgraph.graph.*;

import org.jgrapht.*;
import org.jgrapht.ext.*;
import org.jgrapht.graph.*;


/**
 * A demo applet that shows how to use JGraph to visualize JGraphT graphs.
 *
 * @author Barak Naveh
 * @since Aug 3, 2003
 */
public class StateMachineViewer extends JApplet implements ActionListener
{
    //~ Static fields/initializers ---------------------------------------------

    private static final long serialVersionUID = 3256444702936019250L;
    private static final Color DEFAULT_BG_COLOR = Color.decode("#FAFBFF");
    private static final Dimension DEFAULT_SIZE = new Dimension(530, 320);

    //~ Instance fields --------------------------------------------------------

    //
    private JGraphModelAdapter<Node, TransitionEdge> jgAdapter;
    private ArrayList<Node> states = new ArrayList<Node>();
    private ArrayList<TransitionEdge> transitions= new ArrayList<TransitionEdge>();
    private Machine machine;
    ListenableGraph<Node, TransitionEdge> g;
    
    private static JFrame frame = new JFrame();
    private static JFrame frame2 = new JFrame();
    private static JPanel actions = new JPanel();
    private static JTextField textfield = new JTextField();
    private static JLabel status = new JLabel(); 

    //~ Methods ----------------------------------------------------------------

    /**
     * An alternative starting point for this demo, to also allow running this
     * applet as an application.
     *
     * @param args ignored.
     */
    public static void main(String [] args)
    {
        StateMachineViewer applet = new StateMachineViewer();
        applet.init();

        textfield.setSize(70, 20);
        textfield.setColumns(10);
        actions.add(textfield);
        
        JButton addState = new JButton("Add State");
        addState.setActionCommand("addState");
        addState.addActionListener(applet);
        addState.setSize(70, 20);
        actions.add(addState);
        
        JButton addInitial = new JButton("Add Initial");
        addInitial.setActionCommand("addInitial");
        addInitial.addActionListener(applet);
        addInitial.setSize(70, 20);
        actions.add(addInitial);

        JButton addFinal = new JButton("Add Final");
        addFinal.setActionCommand("addFinal");
        addFinal.addActionListener(applet);
        addFinal.setSize(70, 20);
        actions.add(addFinal);
        
        JButton addTransition = new JButton("Add Transition");
        addTransition.setActionCommand("addTransition");
        addTransition.addActionListener(applet);
        addTransition.setSize(70, 20);
        actions.add(addTransition);
        
        JButton testPath = new JButton("Test Path");
        testPath.setActionCommand("testPath");
        testPath.addActionListener(applet);
        testPath.setSize(70, 20);
        actions.add(testPath);
                
        status.setSize(280, 20);
        status.setForeground(Color.blue);
        actions.add(status);
        
        frame.getContentPane().add(applet);
        frame2.getContentPane().add(actions);
        frame.setTitle("JGraphT Adapter to JGraph Demo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(512, 512);
        frame2.setSize(300, 300);
        frame.setVisible(true);
        frame2.setVisible(true);
    }

    /**
     * {@inheritDoc}
     */
    public void init()
    {
        // create a JGraphT graph
        g = new ListenableDirectedMultigraph<Node, TransitionEdge>(TransitionEdge.class);

        // create a visualization using JGraph, via an adapter
        jgAdapter = new JGraphModelAdapter<Node, TransitionEdge>(g);

        JGraph jgraph = new JGraph(jgAdapter);

        adjustDisplaySettings(jgraph);
        getContentPane().add(jgraph);
        resize(DEFAULT_SIZE);

        try {
			machine = new Machine();
		} catch (CGException e) {
			e.printStackTrace();
		}
    }

    private void adjustDisplaySettings(JGraph jg)
    {
        jg.setPreferredSize(DEFAULT_SIZE);

        Color c = DEFAULT_BG_COLOR;
        String colorStr = null;

        try {
            colorStr = getParameter("bgcolor");
        } catch (Exception e) {
        }

        if (colorStr != null) {
            c = Color.decode(colorStr);
        }

        jg.setBackground(c);
    }

    @SuppressWarnings("unchecked")
    private void positionVertexAt(Object vertex, int x, int y)
    {
        DefaultGraphCell cell = jgAdapter.getVertexCell(vertex);
        AttributeMap attr = cell.getAttributes();
        Rectangle2D bounds = GraphConstants.getBounds(attr);

        Rectangle2D newBounds =
            new Rectangle2D.Double(
                x,
                y,
                bounds.getWidth(),
                bounds.getHeight());

        GraphConstants.setBounds(attr, newBounds);

        AttributeMap cellAttr = new AttributeMap();
        cellAttr.put(cell, attr);
        jgAdapter.edit(cellAttr, null, null, null);
    }

    //~ Inner Classes ----------------------------------------------------------

    /**
     * a listenable directed multigraph that allows loops and parallel edges.
     */
    private static class ListenableDirectedMultigraph<V, E>
        extends DefaultListenableGraph<V, E>
        implements DirectedGraph<V, E>
    {
        private static final long serialVersionUID = 1L;

        ListenableDirectedMultigraph(Class<E> edgeClass)
        {
            super(new DirectedMultigraph<V, E>(edgeClass));
        }
    }
    
    public void actionPerformed(ActionEvent e) {
        try {
        	if ("addState".equals(e.getActionCommand())) {
        		if (textfield.getText().equals("")) {
					status.setText("State name is missing.");
					status.setForeground(Color.red);
					return;
				}
        		status.setText("New state: " + textfield.getText());
        		status.setForeground(Color.blue);
                machine.AddState(states.size(), textfield.getText());
                State st = new State(states.size() + ": " + textfield.getText());
                Node sn = new Node(new StateNode(st));
                states.add(sn);
                g.addVertex(sn);
                positionVertexAt(sn, 100, 100);
                
            } else if ("addInitial".equals(e.getActionCommand())) {
            	status.setText("New state: " + textfield.getText());
        		status.setForeground(Color.blue);
                Initial st = new Initial();
                if(machine.AddInitial(states.size())){
                	Node sn = new Node(new StateNode(st));
                    states.add(sn);
                    g.addVertex(sn);
                    positionVertexAt(sn, 100, 100);
                    status.setText("New initial state: " + st.name);
                } else {
                	status.setText("Only one initial");
                }
                
            	
			} else if ("addFinal".equals(e.getActionCommand())) {
        		status.setForeground(Color.blue);
                Final st = new Final();
                machine.AddFinal(states.size());
                Node sn = new Node(new StateNode(st));
                states.add(sn);
                g.addVertex(sn);
                positionVertexAt(sn, 100, 100);
                status.setText("New final state: " + st.name);
            	
			}else if ("addTransition".equals(e.getActionCommand())){
            	String[] tokens = textfield.getText().split(",");
            	if (tokens.length != 3) {
            		status.setText("Add Transition: <first state>, <second state>, <transition name>");
            		status.setForeground(Color.red);
            		return;
				}
            	
            	status.setText("New transition: " + textfield.getText());
            	status.setForeground(Color.blue);
            	int id1 = Integer.valueOf(tokens[0]);
            	int id2 = Integer.valueOf(tokens[1]);
            	String trigger = tokens[2];
            	
            	machine.addTransition(id1, id2, trigger);
            	TransitionEdge te = new TransitionEdge(trigger);
            	transitions.add(te);
            	g.addEdge(states.get(id1), states.get(id2), te);
            	
            	
            } else if ("testPath".equals(e.getActionCommand())){
            	String[] tokens = textfield.getText().split(",");
            	if (tokens.length <2) {
            		status.setText("Path: <state id>, <first trigger>  [, <second trigger>...]");
            		status.setForeground(Color.red);
            		return;
				}
            	
            	//status.setText("Testing path: " + textfield.getText());
            	
            	int state = Integer.valueOf(tokens[0]);
            	Vector<String> triggers = new Vector<String>();
            	for (int i = 1; i < tokens.length; i++) {
            		triggers.add(tokens[i]);
				}
            	status.setForeground(Color.blue);
            	status.setText("Path: " + 
				((machine.MakePath1(state, triggers)) ? "valid" : "invalid"));
            }
		} catch (Exception e2) {
			e2.printStackTrace();
		} 
        
        textfield.setText("");
    }
}

// End JGraphAdapterDemo.java
