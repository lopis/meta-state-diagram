package view;

import model.State;

public class StateNode{

	private State st;
	
	public StateNode(State s) {
		st = s;
	}

	@Override
	public String toString() {
		return st.name;
	}
}
